import { computed, map } from 'nanostores';

export const $cartStore = map<Record<number, CartItem>>({});

export function addItemToCart(item: ShopItem) {
	const cartItem = $cartStore.get()[item.id];
	const quantity = cartItem ? cartItem.quantity : 0;

	$cartStore.setKey(item.id, {
		item,
		quantity: quantity + 1,
	});
}

export function removeItemFromCart(itemId: number) {
	//@ts-ignore
	$cartStore.setKey(itemId, undefined);
}

export const $subtotalStore = computed($cartStore, (entries) => {
	let subtotal = 0;
	Object.values(entries).forEach((entry) => {
		if (!entry) return;

		subtotal += entry.quantity * entry.item.price;
	});

	return subtotal;
});
