import { defineCollection, z } from 'astro:content';

// https://zod.dev/ for runtime validation
export const collections = {
	blog: defineCollection({
		type: 'content',
		schema: z.object({
			title: z.string(),
			date: z.date(),
			description: z.string().max(200),
		}),
	}),
};
