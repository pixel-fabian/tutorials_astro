import { defineConfig } from 'astro/config';
import solidJs from '@astrojs/solid-js';
import react from '@astrojs/react';

import node from "@astrojs/node";

// https://astro.build/config
export default defineConfig({
  site: 'https://astro-frontend-masters.netlify.app/',
  output: 'hybrid',
  integrations: [solidJs({
    include: ['**/solid/*']
  }), react({
    include: ['**/react/*']
  })],
  adapter: node({
    mode: "standalone"
  })
});